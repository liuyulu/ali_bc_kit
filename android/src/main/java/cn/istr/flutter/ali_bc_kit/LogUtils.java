package cn.istr.flutter.ali_bc_kit;

import android.util.Log;

public class LogUtils {
    public static String TAG = "AlibcModule";

    public static void e(String msg) {
        Log.e(TAG, msg);
    }
}
