import 'package:flutter_test/flutter_test.dart';
import 'package:ali_bc_kit/ali_bc_kit.dart';
import 'package:ali_bc_kit/ali_bc_kit_platform_interface.dart';
import 'package:ali_bc_kit/ali_bc_kit_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockAliBcKitPlatform
    with MockPlatformInterfaceMixin
    implements AliBcKitPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');

  @override
  Future<Map?> checkSession() {
    // TODO: implement checkSession
    throw UnimplementedError();
  }

  @override
  Future<Map?> getUserInfo() {
    // TODO: implement getUserInfo
    throw UnimplementedError();
  }

  @override
  Future<Map?> getUtdid() {
    // TODO: implement getUtdid
    throw UnimplementedError();
  }

  @override
  Future<Map?> init() {
    // TODO: implement init
    throw UnimplementedError();
  }

  @override
  Future<Map?> login() {
    // TODO: implement login
    throw UnimplementedError();
  }

  @override
  Future<Map?> logout() {
    // TODO: implement logout
    throw UnimplementedError();
  }

  @override
  Future<Map?> oauth(url) {
    // TODO: implement oauth
    throw UnimplementedError();
  }

  @override
  Future<Map?> openByBizCode(Map params) {
    // TODO: implement openByBizCode
    throw UnimplementedError();
  }

  @override
  Future<Map?> openByUrl(Map params) {
    // TODO: implement openByUrl
    throw UnimplementedError();
  }

  @override
  Future<Map?> openCart(Map params) {
    // TODO: implement openCart
    throw UnimplementedError();
  }

  @override
  Future<Map?> setChannel(typeName, channelName) {
    // TODO: implement setChannel
    throw UnimplementedError();
  }

  @override
  Future<Map?> setISVVersion(version) {
    // TODO: implement setISVVersion
    throw UnimplementedError();
  }
}

void main() {
  final AliBcKitPlatform initialPlatform = AliBcKitPlatform.instance;

  test('$MethodChannelAliBcKit is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelAliBcKit>());
  });

  test('getPlatformVersion', () async {
    AliBcKit aliBcKitPlugin = AliBcKit();
    MockAliBcKitPlatform fakePlatform = MockAliBcKitPlatform();
    AliBcKitPlatform.instance = fakePlatform;

    expect(await aliBcKitPlugin.getPlatformVersion(), '42');
  });
}
