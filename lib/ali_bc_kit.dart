import 'ali_bc_kit_platform_interface.dart';

class AliBcKit {
  Future<String?> getPlatformVersion() {
    return AliBcKitPlatform.instance.getPlatformVersion();
  }

  // 以下为自定义方法
  Future<Map?> init() {
    return AliBcKitPlatform.instance.init();
  }

  Future<Map?> login() {
    return AliBcKitPlatform.instance.login();
  }

  Future<Map?> logout() {
    return AliBcKitPlatform.instance.logout();
  }

  Future<Map?> getUserInfo() {
    return AliBcKitPlatform.instance.getUserInfo();
  }

  Future<Map?> setChannel(typeName, channelName) {
    return AliBcKitPlatform.instance.setChannel(typeName, channelName);
  }

  Future<Map?> setISVVersion(version) {
    return AliBcKitPlatform.instance.setISVVersion(version);
  }

  Future<Map?> openByBizCode(Map params) {
    return AliBcKitPlatform.instance.openByBizCode(params);
  }

  Future<Map?> openByUrl(Map params) {
    return AliBcKitPlatform.instance.openByUrl(params);
  }

  Future<Map?> openCart(Map params) {
    return AliBcKitPlatform.instance.openCart(params);
  }

  Future<Map?> checkSession() {
    return AliBcKitPlatform.instance.checkSession();
  }

  Future<Map?> getUtdid() {
    return AliBcKitPlatform.instance.getUtdid();
  }

  Future<Map?> oauth(url) {
    return AliBcKitPlatform.instance.oauth(url);
  }
}
