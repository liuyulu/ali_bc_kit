import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'ali_bc_kit_method_channel.dart';

abstract class AliBcKitPlatform extends PlatformInterface {
  /// Constructs a AliBcKitPlatform.
  AliBcKitPlatform() : super(token: _token);

  static final Object _token = Object();

  static AliBcKitPlatform _instance = MethodChannelAliBcKit();

  /// The default instance of [AliBcKitPlatform] to use.
  ///
  /// Defaults to [MethodChannelAliBcKit].
  static AliBcKitPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [AliBcKitPlatform] when
  /// they register themselves.
  static set instance(AliBcKitPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  // 以下为方法定义
  Future<Map?> init() {
    throw UnimplementedError('init() has not been implemented.');
  }

  Future<Map?> login() {
    throw UnimplementedError('login() has not been implemented.');
  }

  Future<Map?> logout() {
    throw UnimplementedError('logout() has not been implemented.');
  }

  Future<Map?> getUserInfo() {
    throw UnimplementedError('getUserInfo() has not been implemented.');
  }

  Future<Map?> setChannel(typeName, channelName) {
    throw UnimplementedError('setChannel() has not been implemented.');
  }

  Future<Map?> setISVVersion(version) {
    throw UnimplementedError('setISVVersion() has not been implemented.');
  }

  Future<Map?> openByBizCode(Map params) {
    throw UnimplementedError('openByBizCode() has not been implemented.');
  }

  Future<Map?> openByUrl(Map params) {
    throw UnimplementedError('openByUrl() has not been implemented.');
  }

  Future<Map?> openCart(Map params) {
    throw UnimplementedError('openCart() has not been implemented.');
  }

  Future<Map?> checkSession() {
    throw UnimplementedError('checkSession() has not been implemented.');
  }

  Future<Map?> getUtdid() {
    throw UnimplementedError('getUtdid() has not been implemented.');
  }

  Future<Map?> oauth(url) {
    throw UnimplementedError('oauth() has not been implemented.');
  }
}
